DROP DATABASE IF EXISTS new_post;
CREATE DATABASE IF NOT EXISTS new_post;
USE new_post;
CREATE TABLE IF NOT EXISTS delivery_point (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    address VARCHAR(45) UNIQUE KEY,
    city VARCHAR(45) NOT NULL DEFAULT 'Lviv',
    point_id VARCHAR(45) UNIQUE KEY
)  ENGINE = INNODB;

CREATE TABLE IF NOT EXISTS client (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(45) NOT NULL DEFAULT "Вася",
    surname VARCHAR(45) NOT NULL,
    middle_name VARCHAR(45),
    package_count INT,
    all_name VARCHAR(45) AS(CONCAT(surname, " ", name, " ", surname))
);

CREATE TABLE IF NOT EXISTS package (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,  
    type ENUM("Kitchen", "Weapon", "Toy", "Fishing"),
    size FLOAT NOT NULL,
    weight FLOAT NOT NULL,
    price DECIMAL(5, 4),
    packaging ENUM("Full", "Without", "Polyethylene")
);

CREATE TABLE IF NOT EXISTS getting_package (
    id INT NOT NULL, 
    got BOOL,
    price DECIMAL,
    sending_package_id INT NOT NULL UNIQUE KEY,
    to_client_id INT NOT NULL,
    to_point_id INT NOT NULL
);

CREATE TABLE IF NOT EXISTS sending_package (
    id INT NOT NULL, 
    sending_id INT NOT NULL UNIQUE KEY,
    from_client_id INT NOT NULL,
    from_point_id INT NOT NULL,
    package_id INT NOT NULL UNIQUE KEY
);

CREATE TABLE IF NOT EXISTS redirected_package (
    id INT NOT NULL, 
    sending_package_id INT NOT NULL,
    to_client_id INT NOT NULL,
    to_point_id INT NOT NULL
);

ALTER TABLE sending_package 
    ADD CONSTRAINT pk_id_package_from_client_point
    PRIMARY KEY (id, from_client_id, from_point_id, package_id);
    
ALTER TABLE sending_package
    MODIFY id INT NOT NULL AUTO_INCREMENT;

ALTER TABLE sending_package 
    ADD CONSTRAINT fk_package_in_road
    FOREIGN KEY (package_id)
    REFERENCES package(id)
    ON DELETE CASCADE     ON UPDATE CASCADE;
    
ALTER TABLE sending_package 
    ADD CONSTRAINT fk_from_point
    FOREIGN KEY (from_point_id)
    REFERENCES delivery_point(id)
    ON DELETE CASCADE     ON UPDATE CASCADE;
    
ALTER TABLE sending_package 
    ADD CONSTRAINT fk_from_client
    FOREIGN KEY (from_client_id)
    REFERENCES client(id)
    ON DELETE CASCADE     ON UPDATE CASCADE;
    
ALTER TABLE redirected_package 
    ADD CONSTRAINT pk_id_sending_package_to_client_point
    PRIMARY KEY (id, sending_package_id, to_client_id, to_point_id);
    
ALTER TABLE redirected_package
    MODIFY id INT NOT NULL AUTO_INCREMENT;
    
ALTER TABLE redirected_package 
    ADD CONSTRAINT fk_sending_package
    FOREIGN KEY (sending_package_id)
    REFERENCES sending_package(id)
    ON DELETE CASCADE     ON UPDATE CASCADE;
    
ALTER TABLE redirected_package 
    ADD CONSTRAINT fk_to_point
    FOREIGN KEY (to_point_id)
    REFERENCES delivery_point(id)
    ON DELETE CASCADE     ON UPDATE CASCADE;
    
ALTER TABLE redirected_package 
    ADD CONSTRAINT fk_to_client
    FOREIGN KEY (to_client_id)
    REFERENCES client(id)
    ON DELETE CASCADE     ON UPDATE CASCADE;
    
    
    
ALTER TABLE getting_package 
    ADD CONSTRAINT pk_id_sending_package_to_client_point
    PRIMARY KEY (id, sending_package_id, to_client_id, to_point_id);
    
ALTER TABLE getting_package
    MODIFY id INT NOT NULL AUTO_INCREMENT;
    
ALTER TABLE getting_package 
    ADD CONSTRAINT fk_sending_package_getting
    FOREIGN KEY (sending_package_id)
    REFERENCES sending_package(id)
    ON DELETE CASCADE     ON UPDATE CASCADE;
    
ALTER TABLE getting_package 
    ADD CONSTRAINT fk_to_point_getting
    FOREIGN KEY (to_point_id)
    REFERENCES delivery_point(id)
    ON DELETE CASCADE     ON UPDATE CASCADE;
    
ALTER TABLE getting_package 
    ADD CONSTRAINT fk_to_client_getting
    FOREIGN KEY (to_client_id)
    REFERENCES client(id)
    ON DELETE CASCADE     ON UPDATE CASCADE;
    
CREATE INDEX sending_id_ix ON sending_package(sending_id);